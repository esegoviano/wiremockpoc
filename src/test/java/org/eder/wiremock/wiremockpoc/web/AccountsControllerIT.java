package org.eder.wiremock.wiremockpoc.web;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import org.eder.wiremock.wiremockpoc.config.WireMockInitializer;
import org.eder.wiremock.wiremockpoc.domain.AccountDTO;
import org.eder.wiremock.wiremockpoc.domain.types.AccountType;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;

import java.util.UUID;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.jupiter.api.Assertions.*;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(initializers = {WireMockInitializer.class})
class AccountsControllerIT {

    private static final UUID ACCOUNT_ID = UUID.fromString("93414e4d-d477-4257-bcbc-3a6bb6ab4b07");

    @Autowired
    private WireMockServer wireMockServer;

    @LocalServerPort
    private Integer port;

    @AfterEach
    public void afterEach() {
        this.wireMockServer.resetAll();
    }

    @Autowired
    AccountsController accountsController;

    @Test
    void greeting() {
        assertEquals(accountsController.greeting(), "Hello");
    }

    @Test
    void testGetAccount() {
        this.wireMockServer.stubFor(
                WireMock.get("/getMyAccount/"+ACCOUNT_ID)
                        .willReturn(aResponse()
                                .withHeader("Content-Type", MediaType.APPLICATION_JSON_VALUE)
                                .withBodyFile("accountResponse.json")
                        )
        );

        AccountDTO result = accountsController.getAccount(ACCOUNT_ID);
        assertAll("account",
                () -> assertEquals(result.getId(), ACCOUNT_ID),
                () -> assertEquals(result.getAccountType(), AccountType.INVESTMENT),
                () -> assertEquals(result.getBalance(), 100L),
                () -> assertEquals(result.getName(), "MyAccount"),
                () -> assertEquals(result.getOwnerList().size(), 2)
        );
    }
}