package org.eder.wiremock.wiremockpoc.web;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.eder.wiremock.wiremockpoc.domain.AccountDTO;
import org.eder.wiremock.wiremockpoc.service.AccountService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;


@Slf4j
@AllArgsConstructor
@RestController
public class AccountsController {

    AccountService accountService;

    @GetMapping("/account/{id}")
    public AccountDTO getAccount(@PathVariable UUID id) {
        log.info("Account id {}", id);
        return accountService.getAccount(id);
    }

    @GetMapping("/hello")
    public String greeting() {
        log.info("Hello");
        return "Hello";
    }
}
