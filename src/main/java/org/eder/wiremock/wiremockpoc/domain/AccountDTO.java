package org.eder.wiremock.wiremockpoc.domain;

import lombok.*;
import org.eder.wiremock.wiremockpoc.domain.types.AccountType;

import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AccountDTO {
    UUID id;
    String name;
    AccountType accountType;
    Long balance;
    List<OwnerDTO> ownerList;
}
