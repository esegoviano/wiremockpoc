package org.eder.wiremock.wiremockpoc.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import org.eder.wiremock.wiremockpoc.domain.types.OwnerType;

import java.time.LocalDate;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OwnerDTO {
    UUID id;

    String fullName;

    @JsonFormat(pattern="yyyy-MM-dd")
    LocalDate birthDate;

    OwnerType type;
}
