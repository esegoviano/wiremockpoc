package org.eder.wiremock.wiremockpoc.domain.types;

public enum OwnerType {
    OWNER,
    DELEGATE
}
