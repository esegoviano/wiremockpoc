package org.eder.wiremock.wiremockpoc.domain.types;

public enum AccountType {
    SAVINGS,
    INVESTMENT,
    BROKER
}
