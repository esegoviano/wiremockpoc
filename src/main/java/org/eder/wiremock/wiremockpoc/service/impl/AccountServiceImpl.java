package org.eder.wiremock.wiremockpoc.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.eder.wiremock.wiremockpoc.domain.AccountDTO;
import org.eder.wiremock.wiremockpoc.service.AccountService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.UUID;

@Slf4j
@Service
public class AccountServiceImpl implements AccountService {

    @Value("${account_url}")
    private String accountUrl;

    @Override
    public AccountDTO getAccount(UUID accountId) {
        log.info("Calling to {}/{} to get the account info", accountUrl,accountId);
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForObject(accountUrl+"/"+accountId, AccountDTO.class);
    }
}
