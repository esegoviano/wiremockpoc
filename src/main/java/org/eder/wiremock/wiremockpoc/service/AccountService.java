package org.eder.wiremock.wiremockpoc.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.eder.wiremock.wiremockpoc.domain.AccountDTO;

import java.util.UUID;

public interface AccountService {

    AccountDTO getAccount(UUID uuid);
}
