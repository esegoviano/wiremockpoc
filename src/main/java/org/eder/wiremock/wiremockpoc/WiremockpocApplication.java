package org.eder.wiremock.wiremockpoc;

import org.eder.wiremock.wiremockpoc.web.AccountsController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
public class WiremockpocApplication {

    public static void main(String[] args) {
        SpringApplication.run(WiremockpocApplication.class, args);
    }

}
